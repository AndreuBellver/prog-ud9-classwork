package com.bellver.prog.A3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MainA3 {

    public static Scanner scanner;

    public static void main(String[] args) {

        leerNumeros();

    }

    public static void leerNumeros() {

        scanner = new Scanner(System.in);
        double maxNumber = 0;
        double number;
        int contadorNumeros = 0;


        do {

            try {

                System.out.println("Introduce un numero:");
                number = scanner.nextDouble();

                if (contadorNumeros == 0) {

                    maxNumber = number;

                } else if (number > maxNumber) {

                    maxNumber = number;

                }

                contadorNumeros++;

            } catch (InputMismatchException failInt) {

                if (contadorNumeros == 0) {

                    System.out.println("No se ha introducido ningun numero.");

                } else {

                    System.out.println("\nMax: " + maxNumber);

                }

                return;

            } finally {

                scanner.nextLine();
            }

        } while (true);

    }
}

