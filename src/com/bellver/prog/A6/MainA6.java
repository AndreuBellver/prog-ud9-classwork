package com.bellver.prog.A6;

public class MainA6 {

    public static void main(String[] args){

        int[] arrayInteger = new int[]{-2,-1,0,1,2};

        dividirEntreArray(arrayInteger,2);

        dividirEntreArray(2, arrayInteger);

    }

    public static void dividirEntreArray(int[] array, int number){

        for(int x = 0; x < array.length; x++){
            try {

                System.out.println(number / array[x] + " ");

            } catch (ArithmeticException e){

                System.out.println("No es pot dividir per 0.");
            }

        }

        System.out.println();
    }

    public static void dividirEntreArray(int number, int[] array){

        for(int x = 0; x < array.length; x++){

                if (array[x]!= 0) {
                    System.out.println(number / array[x] + " ");
                }

        }

    }
}
