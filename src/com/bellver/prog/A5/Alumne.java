package com.bellver.prog.A5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Alumne implements Comparable<Alumne> {

    private String nom;
    private double edad;
    private double altura;

    private Scanner scanner;

    public Alumne(String nom, int edad, double altura) {
        this.nom = nom;
        this.edad = edad;
        this.altura = altura;
    }

    public Alumne() {
        this.nom = getNom();
        this.edad = getEdad();
        this.altura = getAltura();
    }

    private String getNom() {

        scanner = new Scanner(System.in);
        System.out.print("nom: ");
        return new String(scanner.next());

    }

    private double getEdad(){

        double edad;

        do {

            try {

                System.out.println("Edad: ");
                edad = scanner.nextDouble();


                return edad;

            } catch (InputMismatchException a) {

                System.out.println("La edad no puede contener letras.");
                scanner.nextLine();
            }

        } while (true);

    }

    private double getAltura(){

        double altura;

        do {

            try {

                System.out.println("Altura: ");
                altura = scanner.nextDouble();

                return altura;

            } catch (InputMismatchException a) {

                System.out.println("La altura no puede contener letras.");
                scanner.nextLine();
            }

        } while (true);

    }

    @Override
    public String toString() {
        return "Alumne{" +
                "nom='" + nom + '\'' +
                ", edad=" + edad +
                ", altura=" + altura +
                ", scanner=" + scanner +
                '}';
    }

    @Override
    public int compareTo(Alumne o) {

        if (this.edad < o.edad){

            return -1;

        } else if (this.edad > o.edad){

            return 1;

        } else {

            return 0;

        }

    }
}
