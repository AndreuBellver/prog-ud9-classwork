package com.bellver.prog.A5;

public class MainA5 {

    public static void main(String[] args){

       Alumne alumne1 = new Alumne("andreu", 22, 1.74);
       Alumne alumne2 = new Alumne();

       System.out.println(alumne1);
       System.out.println(alumne2);

       System.out.println(alumne1.compareTo(alumne2));
    }
}
