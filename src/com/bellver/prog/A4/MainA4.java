package com.bellver.prog.A4;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MainA4 {

    public static Scanner scanner;

    public static void main(String[] args) {

        int[] newArray = new int[10];

        integersArray(newArray);

    }

    public static void integersArray(int[] newArray) {

        scanner = new Scanner(System.in);

        int contadorNumeros = 0;

        do {

            try {

                System.out.println("Introduce un numero:");
                newArray[contadorNumeros] = scanner.nextInt();

                contadorNumeros++;

                scanner.nextLine();

            } catch (InputMismatchException a) {

                System.out.println("El valor insertado debe de ser un numero.");

            } catch (ArrayIndexOutOfBoundsException a) {

                String cadenaArray = "";

                System.out.println("El array esta completo.");

                for (int x = 0; x < newArray.length; x++) {

                    cadenaArray += newArray[x] + " ";
                }

                System.out.println(cadenaArray);
                return;

            } catch (ExceptionInInitializerError a) {

                System.out.println("La variable no esta inicializada.");
                return;

            }

        } while (true);

    }

}