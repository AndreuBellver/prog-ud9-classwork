package com.bellver.prog.A1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MainA1 {

    public static Scanner scanner;

    public static void main (String[] args){

        System.out.println("Found Numbers:\n" + leerNumero());

    }

    public static String leerNumero(){

        scanner = new Scanner(System.in);

        String insertNumbers = "";

        do {

            try {

                System.out.println("Introduce un numero:");
                int numero = scanner.nextInt();

                if ((numero + "").matches("[1-5]")){

                    return insertNumbers;

                }

                insertNumbers = insertNumbers + numero + " ";

            } catch (InputMismatchException failInt){

                System.out.println("El valor introducido no es valido. Introduce un numero.");

            } finally {

                scanner.nextLine();
            }

        } while (true);



    }

}