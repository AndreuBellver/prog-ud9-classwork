package com.bellver.prog.A2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MainA2 {

    public static Scanner scanner;

    public static void main(String[] args) {

        leer10Numeros();

    }

    public static void leer10Numeros() {

        scanner = new Scanner(System.in);

        double maxNumber = 0;
        double number;

        int contadorNumeros = 0;
        int contadorErrors = 0;

        while (contadorNumeros < 10) {

            try {

                System.out.println("Introduce un numero:");
                number = scanner.nextDouble();

                if (contadorNumeros == 0) {

                    maxNumber = number;

                } else if (number > maxNumber){

                    maxNumber = number;

                }

                contadorNumeros++;

            } catch (InputMismatchException failInt) {

                System.out.println("El valor introducido no es valido. Introduce un numero.");
                contadorErrors++;

            } finally {

                scanner.nextLine();
            }

        }

        System.out.println("Max: " + maxNumber);
        System.out.println("Num de errors introduits: " + contadorErrors);

    }
}

