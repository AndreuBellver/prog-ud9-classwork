package com.bellver.prog.A7;

public class MainA7 {

    public  static void main (String[] args){

        String[] newArray = new String[]{"andreu", "bellver", "ferrando", null};

        System.out.print("Iniciales: " + mostrarCadenesArray(newArray));

    }

    public static String mostrarCadenesArray(String[] array){

        String iniciales = "";

        for (int x = 0; x < array.length; x++) {

            try {

                iniciales += array[x].charAt(0) + ".";

            } catch (NullPointerException e){

                System.out.println("La ultima casilla estaba vacia.");

            }

        }

        return iniciales;
    }
}
