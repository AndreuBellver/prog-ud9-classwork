package com.bellver.prog.A8;

public class MainA8 {

    public static void main (String[] args){

        PersonFactory interfacePerson = new PersonFactory();

        try {

            interfacePerson.create();
            System.out.println("La persona ha sido creada");

        } catch (InvalidInputException e){

            System.out.print("Un error ha ocurrido: " + e.getMessage());

        }

    }

}
