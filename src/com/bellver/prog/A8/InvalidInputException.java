package com.bellver.prog.A8;

public class InvalidInputException extends RuntimeException {

    public InvalidInputException(String message) {

      super(message);

    }

}
