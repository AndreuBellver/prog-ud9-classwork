package com.bellver.prog.A8;

public class Validator {

    private final String CP_MACHES = "[0-4][0-9]{4}$|[5][0-2][0-9]{3}$";
    private final String NIF_MACHES = "[0-9]{8}[A-H J-N P Q S T W-Z]";
    private final String EMAIL_MACHES = "[a-zA-Z0-9]+[@][a-zA-Z0-9]+[.][a-zA-Z]+";

    public void isCPValid (String originalString) {

        if (!originalString.matches(CP_MACHES)) {

            throw new InvalidInputException("Invalid CP.");
        }

    }

    public void isValidNIF (String originalString){

        if (!originalString.matches(NIF_MACHES)){

            throw new InvalidInputException("Invalid DNI.");

        }

    }

    public void isValidEmail (String originalString){

        if (!originalString.matches(EMAIL_MACHES)) {

            throw new InvalidInputException("Invalid E-mail.");

        }

    }

}

