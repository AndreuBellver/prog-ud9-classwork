package com.bellver.prog.A8;

import java.util.Scanner;

public class PersonFactory {

    private Scanner scanner;
    private Validator validator;
    private String[] errorList;

    public PersonFactory() {

        scanner = new Scanner(System.in);
        validator = new Validator();
        errorList = new String[]{"El dni es incorrecto.", "El email es incorrecto.", "El codigo postal es incorrecto."};

    }


    public Person create() {

        System.out.println("~~Person Register~~");
        System.out.println("===================");

        Person newPerson = new Person(getNom(),getCognom(),getDni(),getEmail(),getAdreça(),getCodigoPostal());
        return newPerson;

    }

    private String getNom() {

        System.out.println("Nom:");
        return scanner.next();
    }

    private String getCognom() {

        System.out.println("Cognom:");
        return scanner.next();

    }

    private String getDni() {

        System.out.println("DNI:");
        String dni = scanner.next();
        this.validator.isValidNIF(dni);
        return dni;

    }

    private String getEmail() {

        System.out.println("E-mail:");
        String email = scanner.next();
        this.validator.isValidEmail(email);
        return email;

    }

    private String getAdreça() {

        System.out.println("Adreça:");
        return scanner.next();

    }

    private String getCodigoPostal() {

        String cP;

        System.out.println("Codigo Postal:");
        cP = scanner.next();
        this.validator.isCPValid(cP);
        return cP;

    }
}
