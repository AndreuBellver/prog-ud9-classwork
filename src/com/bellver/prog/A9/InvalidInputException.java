package com.bellver.prog.A9;

public class InvalidInputException extends Exception {

    private String[] messageErrors;

    public InvalidInputException(String[] messageErrors) {

        this.messageErrors = messageErrors;

    }

    public String[] getMessageErrors() {

        return messageErrors;
    }
}
