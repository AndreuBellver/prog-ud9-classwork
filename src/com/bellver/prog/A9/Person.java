package com.bellver.prog.A9;

public class Person {

        private String nom;
        private String cognom;
        private String dni;
        private String email;
        private String adreça;
        private String codigoPostal;

        public Person(String nom, String cognom, String dni, String email, String adreça, String codigoPostal) {
            this.nom = nom;
            this.cognom = cognom;
            this.dni = dni;
            this.email = email;
            this.adreça = adreça;
            this.codigoPostal = codigoPostal;

        }

    @Override
        public String toString() {

            return ("Nom: " + nom +
                    "\nCognom: " + cognom +
                    "\nDNI: " + dni +
                    "\nemail: " + email +
                    "\nCodic Postal: " + codigoPostal +
                    "\nadreça: " + adreça
            );
        }
    }

