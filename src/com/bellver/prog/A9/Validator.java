package com.bellver.prog.A9;

public class Validator {

    private final String CP_MACHES = "[0-4][0-9]{4}$|[5][0-2][0-9]{3}$";
    private final String NIF_MACHES = "[0-9]{8}[A-H J-N P Q S T W-Z]";
    private final String EMAIL_MACHES = "[a-zA-Z0-9]+[@][a-zA-Z0-9]+[.][a-zA-Z]+";

    public boolean isCPValid (String originalString) {

        return (originalString.matches(CP_MACHES));

    }

    public boolean isValidNIF (String originalString){

        return (originalString.matches(NIF_MACHES));

    }

    public boolean isValidEmail (String originalString){

        return (originalString.matches(EMAIL_MACHES));

    }

}

