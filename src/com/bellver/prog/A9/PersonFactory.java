package com.bellver.prog.A9;

import java.util.ArrayList;
import java.util.Scanner;

public class PersonFactory {

    private Scanner scanner;
    private Validator validator;
    private ArrayList<String> errorList;

    public PersonFactory() {

        scanner = new Scanner(System.in);
        validator = new Validator();
        errorList = new ArrayList<>();

    }


    public Person create() throws InvalidInputException {

        System.out.println("~~Person Register~~");
        System.out.println("===================");

        Person newPerson = new Person(getNom(),getCognom(),getDni(),getEmail(),getAdreça(),getCodigoPostal());

       assertNoError();

       return newPerson;

    }

    private String getNom() {

        System.out.println("Nom:");
        return scanner.next();
    }

    private String getCognom() {

        System.out.println("Cognom:");
        return scanner.next();

    }

    private String getDni() {

        System.out.println("DNI:");
        String dni = scanner.next();

        if (!validator.isValidNIF(dni)){

            setError("Dni Incorrecto.");

        }

        return dni;

    }

    private String getEmail() {

        System.out.println("E-mail:");
        String email = scanner.next();

        if (!validator.isValidEmail(email)){

            setError("Email incorrecto.");

        }

        return email;

    }

    private String getAdreça() {

        System.out.println("Adreça:");
        return scanner.next();

    }

    private String getCodigoPostal() {

        System.out.println("Codigo Postal:");
        String cP = scanner.next();

        if (!validator.isCPValid(cP)){

          setError("Codigo postal Incorrecto.");

        }

        return cP;

    }

    private void setError(String error){

        this.errorList.add(error);

    }

    private void assertNoError() throws InvalidInputException {

        if (!errorList.isEmpty()){

            String[] errors = new String[errorList.size()];
            errors = errorList.toArray(errors);
            throw new InvalidInputException(errors);

        }

    }
}
