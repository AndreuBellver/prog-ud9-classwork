package com.bellver.prog.A9;

public class MainA8 {

    public static void main (String[] args){

        PersonFactory interfacePerson = new PersonFactory();

        try {

            interfacePerson.create();

        } catch (InvalidInputException e){

            System.out.println("Errors:\n");
            showErrorList(e.getMessageErrors());

            }

        }

   public static void showErrorList(String[] errors){

        for (String error:errors) {

            System.out.println(error);

        }

    }

}
